# Phantas0s Arch Install

[![Mousless Development Environment](screen_780.png)](screen.png)


This is my **personal install scripts** to install my whole Mouseless Development Environment. 

If you're searching the scripts related to my book Building Your Mouseelss Development Environment, it's there: https://github.com/Phantas0s/mouseless-book-companion/tree/master/part_III/05_user_installer/arch_installer

I would recommend to install Arch by yourself first. You'll learn quite a lot from it; then you can try to create your own scripts or use these. 

1. The entry point is the file `install_sys.sh`.
2. Every software in `apps.csv` will be installed. The first column is the group you can choose during installation, the second column is the package names, and the third own is the description FYI. Every packagees from the group selected during installation will be installed.

## Building Your Mouseless Development Environment

Switching between a keyboard and mouse costs cognitive energy. [My book will help you set up a Linux-based development environment](https://themouseless.dev) that keeps your hands on your keyboard. Take the brain power you've been using to juggle input devices and focus it where it belongs: on the things you create.

You'll learn how to write your own installation scripts too!
